package de.rwth.jne.teaching;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RingBufferTest {
	IRingBuffer buffer;

	@BeforeEach
	void setUp() throws Exception {
		this.buffer = new TutRingBuffer(5);
	}

	@Test
	void test_basicFunctionality() {
		assertTrue(this.buffer.isEmpty());
		this.buffer.write(1);
		this.buffer.write(2);
		this.buffer.write(3);
		this.buffer.write(4);
		this.buffer.write(5);
		assertTrue(this.buffer.isFull());
		assertEquals(this.buffer.read(), 1);
		assertEquals(this.buffer.read(), 2);
		assertEquals(this.buffer.read(), 3);
		assertEquals(this.buffer.read(), 4);
		assertEquals(this.buffer.read(), 5);
		assertTrue(this.buffer.isEmpty());
	}
	
	@Test
	void test_notFull() {
		assert(this.buffer.isEmpty());
		this.buffer.write(1);
		this.buffer.write(2);
		this.buffer.write(3);
		assertFalse(this.buffer.isFull());
		assertFalse(this.buffer.isEmpty());
		assertEquals(this.buffer.read(), 1);
		assertEquals(this.buffer.read(), 2);
		assertEquals(this.buffer.read(), 3);
		assertTrue(this.buffer.isEmpty());
	}
	
	@Test
	void test_overrideBufferOnce() {
		this.buffer.write(1);
		this.buffer.write(2);
		this.buffer.write(3);
		this.buffer.write(4);
		this.buffer.write(5);
		assertTrue(this.buffer.isFull());
		this.buffer.write(6);
		assertFalse(this.buffer.isEmpty());
		assertTrue(this.buffer.isFull());
		assertEquals(this.buffer.read(), 2);
		assertEquals(this.buffer.read(), 3);
		assertEquals(this.buffer.read(), 4);
		assertEquals(this.buffer.read(), 5);
		assertEquals(this.buffer.read(), 6);
		assertTrue(this.buffer.isEmpty());
	}
	
	@Test
	void test_fullyOverrideBuffer() {
		this.buffer.write(1);
		this.buffer.write(2);
		this.buffer.write(3);
		this.buffer.write(4);
		this.buffer.write(5);
		assertTrue(this.buffer.isFull());
		this.buffer.write(6);
		this.buffer.write(7);
		this.buffer.write(8);
		this.buffer.write(9);
		this.buffer.write(10);
		assertFalse(this.buffer.isEmpty());
		assertTrue(this.buffer.isFull());
		assertEquals(this.buffer.read(), 6);
		assertEquals(this.buffer.read(), 7);
		assertEquals(this.buffer.read(), 8);
		assertEquals(this.buffer.read(), 9);
		assertEquals(this.buffer.read(), 10);
		assertTrue(this.buffer.isEmpty());
	}

}
