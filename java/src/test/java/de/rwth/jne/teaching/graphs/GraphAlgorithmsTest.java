package de.rwth.jne.teaching.graphs;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class GraphAlgorithmsTest {
	
	@Test
	void test_DFS() {
		final int size = 5;
		// Generics don't generally work nicely together with arrays
		@SuppressWarnings("unchecked")
		List<Integer>[] adj = new LinkedList[size];
		
		for (int i = 0; i < adj.length; i++) {
			adj[i] = new LinkedList<Integer>();
		}
		
		adj[0].add(4);
		
		adj[1].add(1);
		adj[1].add(2);
		
		adj[2].add(3);
		adj[2].add(1);
		
		adj[3].add(2);
		adj[4].add(1);
		
		assertEquals(Arrays.asList(0, 4, 1, 2, 3), GraphAlgorithms.DFS(adj, 0));
	}
	
	@Test
	void test_critialPath() {
		final int size = 5;
		// Generics don't generally work nicely together with arrays
		@SuppressWarnings("unchecked")
		List<Integer>[] adj = new LinkedList[size];
		int[] duration = new int[size];
		
		for (int i = 0; i < adj.length; i++) {
			adj[i] = new LinkedList<Integer>();
		}
		
		adj[1].add(2);
		
		adj[2].add(4);
		
		adj[3].add(4);
		
		duration[0] = 1;
		duration[1] = 1;
		duration[2] = 1;
		duration[3] = 5;
		duration[4] = 1;
		
		assertEquals(Arrays.asList(3, 4), GraphAlgorithms.criticalPath(adj, duration));
		
		adj[0].add(3);
		adj[0].add(1);
		
		assertEquals(Arrays.asList(0, 3, 4), GraphAlgorithms.criticalPath(adj, duration));
	}

}
