package de.rwth.jne.teaching;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ListTest {
	IList list;
        Element[] elements = new Element[4];

	@BeforeEach
	void setUp() throws Exception {
                Element e = new Element();
                e.k = 3;
                elements[0] = e;

                e = new Element();
                e.k = 4;
                elements[1] = e;

                e = new Element();
                e.k = 5;
                elements[2] = e;

                e = new Element();
                e.k = 4;
                elements[3] = e;
	}

	@Test
	void test_singleList() {
                list = new SinglyLinkedList();
                list.append(elements[0]);
                list.append(elements[1]);
                list.append(elements[2]);
                list.append(elements[3]);

		assertNotNull(list.search(3));
		assertNotNull(list.search(4));
		assertNotNull(list.search(5));
		assertEquals(list.predecessor(4), elements[2]);
		assertEquals(list.predecessor(5), elements[1]);
	}
	
	@Test
	void test_doubleList() {
                list = new DoublyLinkedList();
                list.append(elements[0]);
                list.append(elements[1]);
                list.append(elements[2]);
                list.append(elements[3]);

		assertNotNull(list.search(3));
		assertNotNull(list.search(4));
		assertNotNull(list.search(5));
		assertEquals(list.predecessor(4), elements[2]);
		assertEquals(list.predecessor(5), elements[1]);
	}
	
}
