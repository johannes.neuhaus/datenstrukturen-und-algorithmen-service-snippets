package de.rwth.jne.teaching.graphs;

public enum Colour {
	GREY,
	BLACK,
	WHITE
}
