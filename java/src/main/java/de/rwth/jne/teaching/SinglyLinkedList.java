package de.rwth.jne.teaching;

public class SinglyLinkedList implements IList {
    public Element head = null;

    public Element predecessor(int key) {
        Element pre = null;
        Element current = head;
        while (current != null) {
            if (current.next != null && current.next.k == key) {
                pre = current;
            }
            
            current = current.next;
        }

        return pre;
    }

    public Element search(int key) {
        Element current = head;
        while (current != null) {
            if (key == current.k) return current;

            current = current.next;
        }

        return null;
    }

    public void append(Element e) {
        if (head == null) {
            head = e;
            return;
        }

        Element current = head;
        while (current.next != null) {
            current = current.next;
        }

        current.next = e;
    }
}
