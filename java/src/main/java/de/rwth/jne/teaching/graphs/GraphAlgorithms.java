package de.rwth.jne.teaching.graphs;

import java.util.LinkedList;
import java.util.List;

public class GraphAlgorithms {
	
	private GraphAlgorithms() {}
	
	public static List<Integer> DFS(List<Integer>[] adj, int start, Colour[] colours) {
		colours[start] = Colour.GREY;
		
		List<Integer> visited = new LinkedList<>();
		visited.add(start);
		
		for (Integer node : adj[start]) {
			if (colours[node] == Colour.WHITE) {
				visited.addAll(DFS(adj, node, colours));
			}
		}
		
		colours[start] = Colour.BLACK;
		
		return visited;
	}
	
	public static List<Integer> DFS(List<Integer>[] adj, int start) {
		Colour[] colours = new Colour[adj.length];
		
		for (int i = 0; i < adj.length; i++) {
			colours[i] = Colour.WHITE;
		}
		
		return DFS(adj, start, colours);
	}
	
	public static void criticalPath(List<Integer>[] adj, int start, Colour[] colours, int[] duration, int[] times, int[] maxNeighbour) {
		colours[start] = Colour.GREY;
			
		int maximalNeigbourTime = 0;
		
		for (Integer node : adj[start]) {
			if (colours[node] == Colour.WHITE) {
				criticalPath(adj, node, colours, duration, times, maxNeighbour);
			}
			
			if (maximalNeigbourTime < times[node]) {
				maximalNeigbourTime = times[node];
				maxNeighbour[start] = node;
			}
		}
		
		// start node has visited all neighbours
		colours[start] = Colour.BLACK;
		
		times[start] = maximalNeigbourTime + duration[start];
		
	}
	
	public static List<Integer> criticalPath(List<Integer>[] adj, int[] duration) {
		int[] times = new int[adj.length];
		Colour[] colours = new Colour[adj.length];
		int[] maxNeighbours = new int[adj.length];
		for (int i = 0; i < times.length; i++) {
			times[i] = 0;
			colours[i] = Colour.WHITE;
			maxNeighbours[i] = -1;
		}
		
		int pathTime = 0;
		int start = -1;
		for (int i = 0; i < adj.length; i++) {
			// this iteration is only necessary for the case: 1 -> 2 <- 3
			if (colours[i] != Colour.WHITE) continue;
			
			criticalPath(adj, i, colours, duration, times, maxNeighbours);
						
			if (times[i] > pathTime) {
				start = i;
			}
		}
		
		for (int i = 0; i < times.length; i++) {
			System.out.println("node " + i + " has time " + times[i]);
		}
		
		for (int i = 0; i < maxNeighbours.length; i++) {
			System.out.println("node " + i + " has maxNeighbor " + maxNeighbours[i]);
		}
		
		int current = start;
		List<Integer> criticalPath = new LinkedList<>();
		while (current >= 0) {
			criticalPath.add(current);
			current = maxNeighbours[current];
		}
		
		return criticalPath;
	}


}
