package de.rwth.jne.teaching;

public class TutRingBuffer implements IRingBuffer {
	
	private int[] elements;
	private int mostRecentIndex;
	private int oldIndex;
	private int capacity;
	
	public TutRingBuffer(int capacity) {
		this.capacity = capacity + 1;
		this.mostRecentIndex = 0;
		this.oldIndex = 0;
		this.elements = new int[capacity + 1];
	}

	@Override
	public boolean isEmpty() {
		return this.mostRecentIndex == this.oldIndex;
	}

	@Override
	public boolean isFull() {
		return (this.mostRecentIndex + 1) % capacity == this.oldIndex;
	}

	@Override
	public int read() {
		int result = elements[this.oldIndex];
		this.oldIndex = (this.oldIndex + 1) % this.capacity;
		return result;
	}

	@Override
	public void write(int number) {
		this.elements[this.mostRecentIndex] = number;
		this.mostRecentIndex = (this.mostRecentIndex + 1) % this.capacity;
		
		if (this.isEmpty()) this.oldIndex = (this.oldIndex + 1) % this.capacity;
	}

}
