package de.rwth.jne.teaching;

public interface IRingBuffer {
	
	/**
	 * Check whether the RingBuffer isEmpty
	 * 
	 * @return true if buffer is empty
	 */
	boolean isEmpty();
	
	/**
	 * Check whether the RingBuffer is full
	 * 
	 * @return
	 */
	boolean isFull();
	
	/**
	 * Reads the oldest element from the RingBuffer
	 * 
	 * @return
	 */
	int read();
	
	/**
	 * Write number into RingBuffer. Will override the oldest element if buffer is full
	 * 
	 * @param number to write into array
	 */
	void write(int number);
	
}
