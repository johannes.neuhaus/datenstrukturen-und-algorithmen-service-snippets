package de.rwth.jne.teaching;

public interface IList {
    /**
     * gibt das (erste) Element aus der Liste mit Schlüssel k zurück und null
     * falls es kein derartiges Element gibt.
     */
    public Element search(int k);

    /**
     * gibt das Vorgängerelement von dem letzten Element mit Schlüssel k in
     * der Liste zurück.
     *
     * Vorbedingung: Ein Element mit Schlüssel k ist in der Liste
     */
    public Element predecessor(int k);

    public void append(Element e);
}
