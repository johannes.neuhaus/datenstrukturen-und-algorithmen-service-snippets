package de.rwth.jne.teaching;

public class DoublyLinkedList implements IList {
    private Element head;
    private Element tail;

    public Element predecessor(int key) {
        Element current = tail;

        while (current != null) {
            if (current.k == key) {
                return current.pre;
            }
            current = current.pre;
        }

        return null;
    }

    public Element search(int key) {
        Element current = head;
        
        while (current.next != null) {
            if (current.k == key) return current;

            current = current.next;
        }

        return null;
    }

    public void append(Element e) {
        if (head == null) {
            head = e;
            tail = e;
            return;
        }

        Element current = head;
        while (current.next != null) {
            current = current.next;
        }

        current.next = e;
        e.pre = current;
        tail = e;
    }
}
