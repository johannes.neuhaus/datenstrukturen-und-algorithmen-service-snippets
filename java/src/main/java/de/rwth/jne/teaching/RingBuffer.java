package de.rwth.jne.teaching;

public class RingBuffer implements IRingBuffer {
	
	int[] elements;
	int mostRecentIndex;
	int oldestIndex;
	int capacity;
	
	public RingBuffer(int capacity) {
		this.capacity = capacity + 1;
		this.elements = new int[this.capacity];
		this.mostRecentIndex = 0;
		this.oldestIndex = 0;
	}

	@Override
	public boolean isEmpty() {
		return this.mostRecentIndex == this.oldestIndex;
	}

	@Override
	public boolean isFull() {
		// is the next cell the same as our oldest cell?
		return (this.mostRecentIndex + 1) % this.capacity == this.oldestIndex;
	}

	@Override
	public int read() {
		Integer result = this.elements[this.oldestIndex];
		this.oldestIndex = (this.oldestIndex + 1) % this.capacity;
		return result;
	}

	@Override
	public void write(int number) {
		this.elements[this.mostRecentIndex] = number;
		this.mostRecentIndex = (this.mostRecentIndex + 1) % this.capacity;
		
		// if a buffer seems to be empty after writing we must have overridden an element!
		if(this.isEmpty()) this.oldestIndex = (this.oldestIndex + 1) % capacity;
	}
	
}
