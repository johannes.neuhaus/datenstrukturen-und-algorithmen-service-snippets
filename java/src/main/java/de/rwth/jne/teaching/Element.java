package de.rwth.jne.teaching;

public class Element {
    public int k;
    public Element next = null;

    // Nur für doppelte verkettung
    public Element pre = null;
}
